"""
Model exported as python.
Name : parc_5000_fibre_100
Group : 
With QGIS : 31802
Auteur : Bruno Defrance
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterVectorLayer
from qgis.core import QgsProcessingParameterFeatureSink
from qgis.core import QgsProcessingParameterBoolean
import processing


class Parc_5000_fibre_100(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterVectorLayer('Fibreoptique', 'Fibre optique', defaultValue=None))
        self.addParameter(QgsProcessingParameterVectorLayer('Parcelles', 'Parcelles', defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('Parcelles_5000_fibre_100', 'parcelles_5000_fibre_100', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(4, model_feedback)
        results = {}
        outputs = {}

        # parcelle sup 5000
        alg_params = {
            'EXPRESSION': ' $area > 5000',
            'INPUT': parameters['Parcelles'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['ParcelleSup5000'] = processing.run('native:extractbyexpression', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Fibre en service
        alg_params = {
            'EXPRESSION': ' \"etat\" =  \'En service\' ',
            'INPUT': parameters['Fibreoptique'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['FibreEnService'] = processing.run('native:extractbyexpression', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # Buffer
        alg_params = {
            'DISSOLVE': True,
            'DISTANCE': 100,
            'END_CAP_STYLE': 0,
            'INPUT': outputs['FibreEnService']['OUTPUT'],
            'JOIN_STYLE': 0,
            'MITER_LIMIT': 2,
            'SEGMENTS': 5,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['Buffer'] = processing.run('native:buffer', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # Intersection
        alg_params = {
            'INPUT': outputs['ParcelleSup5000']['OUTPUT'],
            'INPUT_FIELDS': [''],
            'OVERLAY': outputs['Buffer']['OUTPUT'],
            'OVERLAY_FIELDS': [''],
            'OVERLAY_FIELDS_PREFIX': '',
            'OUTPUT': parameters['Parcelles_5000_fibre_100']
        }
        outputs['Intersection'] = processing.run('native:intersection', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['Parcelles_5000_fibre_100'] = outputs['Intersection']['OUTPUT']
        return results

    def name(self):
        return 'parc_5000_fibre_100'

    def displayName(self):
        return 'parc_5000_fibre_100'

    def group(self):
        return ''

    def groupId(self):
        return ''

    def createInstance(self):
        return Parc_5000_fibre_100()
