Bonjour,  

J'ai pris en compte votre demande.  
Le ticket porte le numéro %{ISSUE_ID}

Si cette demande peut servir à d'autres étudiants, je rendrai visible ce ticket.

Merci  
Bruno Defrance

---

Hello,  

I have taken into account your request.  
The ticket number is %{ISSUE_ID}

If this request may be used by other students, this ticket might be visible.

Thank you  
Bruno Defrance
