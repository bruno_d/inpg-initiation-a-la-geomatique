# initiation à la géomatique

(et initiation à l'usage de QGIS)

Ce cours est dispensé dans le cadres de la formation [TEET](https://ense3.grenoble-inp.fr/fr/formation/mastere-specialise-transition-energetique-et-environnementale-des-territoires-teet#page-presentation) & [Smart City](https://ense3.grenoble-inp.fr/fr/formation/smart-cities-sem-m1-sgb-4eus4hat) de l'[ENSE3](https://ense3.grenoble-inp.fr/fr/formation/ingenieur-de-grenoble-inp-ense3-filiere-systemes-energetiques-et-marches-sem#page-presentation), [INP Grenoble](https://www.grenoble-inp.fr/), ainsi que Sciences PO Grenoble : [Transitions ecologiques](http://www.sciencespo-grenoble.fr/formation/transitions-ecologiques/)

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
